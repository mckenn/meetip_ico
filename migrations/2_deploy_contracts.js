const MeeTipCrowdsale = artifacts.require("MeeTipCrowdsale");

module.exports = function(deployer, network, accounts) {
  const startTime = web3.eth.getBlock(web3.eth.blockNumber).timestamp + 1 // one second in the future
  const endTimePreICO = startTime + (7 * 86400 * 7) // 7 days
  const startTimeICO = endTimePreICO + (7 * 86400)
  const endFirstDayICO = startTimeICO + 86400
  const endFirstWeekICO = startTimeICO + (7 * 86400)
  const endSecondWeekICO = startTimeICO + (14 * 86400)
  const endThirdWeekICO = startTimeICO + (21 * 86400)
  const endTime = startTimeICO + (28 * 86400)
  const contributionWallet = accounts[0]
  const foundersWallet = accounts[1]
  const bountyWallet = accounts[2]

  deployer.deploy(MeeTipCrowdsale,
    startTime,
    endTimePreICO,
    startTimeICO,
    endFirstDayICO,
    endFirstWeekICO,
    endSecondWeekICO,
    endThirdWeekICO,
    endTime,
    contributionWallet,
    foundersWallet,
    bountyWallet
  )
};
