pragma solidity ^0.4.11;

import 'zeppelin-solidity/contracts/token/MintableToken.sol';
import 'zeppelin-solidity/contracts/token/BurnableToken.sol';


contract MeeTip is MintableToken, BurnableToken {
    string public name = "MeeTip";
    string public symbol = "MTIP";
    uint256 public decimals = 18;
}
