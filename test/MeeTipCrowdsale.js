import ether from './helpers/ether'
import {advanceBlock} from './helpers/advanceToBlock'
import {increaseTimeTo, duration} from './helpers/increaseTime'
import latestTime from './helpers/latestTime'
import EVMThrow from './helpers/EVMThrow'

const BigNumber = web3.BigNumber

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should()

const MeeTipCrowdsale = artifacts.require('MeeTipCrowdsale')
const MeeTip = artifacts.require('MeeTip')

contract('MeeTipCrowdsale', function ([owner, investor, contributionWallet, foundersWallet, bountyWallet, purchaser]) {

    const PREICO_RATE = new BigNumber(900);
    const FIRST_DAY_ICO_RATE = new BigNumber(800);
    const FIRST_WEEK_ICO_RATE = new BigNumber(750);
    const SECOND_WEEK_ICO_RATE = new BigNumber(700);
    const THIRD_WEEK_ICO_RATE = new BigNumber(650);
    const FOURTH_WEEK_ICO_RATE = new BigNumber(600);
    const MINIMAL_PURCHASE = ether(0.05);
    const TOKENGOAL = new BigNumber(50000e18); // 50 000.0 MTIP
    const ETH_GOAL_PREICO = TOKENGOAL.div(PREICO_RATE).ceil(); // ~55.56 Ether
    const ETH_GOAL_ICO = TOKENGOAL.div(FOURTH_WEEK_ICO_RATE).ceil(); // ~83.34 Ether
    const TOKEN_CAP = new BigNumber(27e24); // 27 000 000.0 MTIP
    const ETH_CAP_PREICO  = TOKEN_CAP.div(PREICO_RATE); // 30000 Ether

    before(async function() {
      //Advance to the next block to correctly read time in the solidity "now" function interpreted by testrpc
      await advanceBlock()
    })

    beforeEach(async function () {
      this.startTime = latestTime() + duration.weeks(1);
      this.endTimePreICO = this.startTime + duration.weeks(1);
      this.afterPreICO = this.endTimePreICO + duration.seconds(1);
      this.startTimeICO = this.endTimePreICO + duration.weeks(1);
      this.endFirstDayICO = this.startTimeICO + duration.days(1);
      this.endFirstWeekICO = this.startTimeICO + duration.weeks(1);
      this.endSecondWeekICO = this.startTimeICO + duration.weeks(2);
      this.endThirdWeekICO = this.startTimeICO + duration.weeks(3);
      this.endTime =   this.startTimeICO + duration.weeks(4);
      this.afterEndTime = this.endTime + duration.seconds(1)

      this.crowdsale = await MeeTipCrowdsale.new(this.startTime,
        this.endTimePreICO, this.startTimeICO, this.endFirstDayICO,
        this.endFirstWeekICO, this.endSecondWeekICO, this.endThirdWeekICO,
        this.endTime, contributionWallet, foundersWallet, bountyWallet)

      this.token = MeeTip.at(await this.crowdsale.token())
    })


    it('should create crowdsale with correct parameters', async function () {
      this.crowdsale.should.exist;
      this.token.should.exist;

      (await this.crowdsale.startTime()).should.be.bignumber.equal(this.startTime);
      (await this.crowdsale.endTimePreICO()).should.be.bignumber.equal(this.endTimePreICO);
      (await this.crowdsale.startTimeICO()).should.be.bignumber.equal(this.startTimeICO);
      (await this.crowdsale.endFirstDayICO()).should.be.bignumber.equal(this.endFirstDayICO);
      (await this.crowdsale.endFirstWeekICO()).should.be.bignumber.equal(this.endFirstWeekICO);
      (await this.crowdsale.endSecondWeekICO()).should.be.bignumber.equal(this.endSecondWeekICO);
      (await this.crowdsale.endThirdWeekICO()).should.be.bignumber.equal(this.endThirdWeekICO);
      (await this.crowdsale.endTime()).should.be.bignumber.equal(this.endTime);
      (await this.crowdsale.rate()).should.be.bignumber.equal(PREICO_RATE);
      (await this.crowdsale.wallet()).should.be.bignumber.equal(contributionWallet);
      (await this.crowdsale.foundersWallet()).should.be.bignumber.equal(foundersWallet);
      (await this.crowdsale.bountyWallet()).should.be.bignumber.equal(bountyWallet);

      // MTIP token
      (await this.token.name()).should.equal("MeeTip");
      (await this.token.decimals()).should.be.bignumber.equal(18);
      (await this.token.symbol()).should.equal("MTIP");
      (await this.crowdsale.bountyWallet()).should.be.bignumber.equal(bountyWallet);

    });

    it('should allocate tokens for founders and bounty program', async function () {
      const expectedTokenAmount = new BigNumber('1500000e18');

      (await this.token.balanceOf(foundersWallet)).should.be.bignumber.equal(expectedTokenAmount);
      (await this.token.balanceOf(bountyWallet)).should.be.bignumber.equal(expectedTokenAmount);
    });

    it('should allow token owner to burn tokens', async function () {
      const balanceBeforeBurn = await this.token.balanceOf(bountyWallet);
      console.log(balanceBeforeBurn);
      const totalSupplyBeforeBurn = await this.token.totalSupply();

      await this.token.burn(1, { from: bountyWallet });

      const balanceAfterBurn = await this.token.balanceOf(bountyWallet);
      const totalSupplyAfterBurn = await this.token.totalSupply();

      balanceBeforeBurn.minus(balanceAfterBurn).should.be.bignumber.equal(1);
      totalSupplyBeforeBurn.minus(totalSupplyAfterBurn).should.be.bignumber.equal(1);
    });

    it('should accept and reject payments according to ICO schedule', async function () {
      // reject before preICO
      await this.crowdsale.send(MINIMAL_PURCHASE).should.be.rejectedWith(EVMThrow);
      await this.crowdsale.buyTokens(investor, {from: investor, value: ether(1)}).should.be.rejectedWith(EVMThrow);

      // accept during preICO
      await increaseTimeTo(this.startTime);
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor}).should.be.fulfilled;

      // reject after preICO and before ICO
      await increaseTimeTo(this.afterPreICO);
      await this.crowdsale.send(MINIMAL_PURCHASE).should.be.rejectedWith(EVMThrow);
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor}).should.be.rejectedWith(EVMThrow);

      // accept during ICO
      await increaseTimeTo(this.startTimeICO);
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor}).should.be.fulfilled;

      // reject after ICO
      await increaseTimeTo(this.afterEndTime);
      await this.crowdsale.send(MINIMAL_PURCHASE).should.be.rejectedWith(EVMThrow);
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor}).should.be.rejectedWith(EVMThrow);

    });

    it('should change conversion rate according to ICO stage', async function () {
      // preICO
      await increaseTimeTo(this.startTime);
      var beforeContribution = (await this.token.balanceOf(investor));
      await this.crowdsale.sendTransaction({value: MINIMAL_PURCHASE, from: investor, gasPrice: 0});
      var afterContribution = (await this.token.balanceOf(investor));
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(PREICO_RATE.mul(MINIMAL_PURCHASE));

      // first day of ICO
      await increaseTimeTo(this.startTimeICO);
      beforeContribution = (await this.token.balanceOf(investor));
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor});
      afterContribution = (await this.token.balanceOf(investor));
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(FIRST_DAY_ICO_RATE.mul(MINIMAL_PURCHASE));

      // first week of ICO
      await increaseTimeTo(this.endFirstDayICO + duration.seconds(1));
      beforeContribution = (await this.token.balanceOf(investor));
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor});
      afterContribution = (await this.token.balanceOf(investor));
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(FIRST_WEEK_ICO_RATE.mul(MINIMAL_PURCHASE));

      // second week of ICO
      await increaseTimeTo(this.endFirstWeekICO + duration.seconds(1));
      beforeContribution = (await this.token.balanceOf(investor));
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor});
      afterContribution = (await this.token.balanceOf(investor));
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(SECOND_WEEK_ICO_RATE.mul(MINIMAL_PURCHASE));

      // third week of ICO
      await increaseTimeTo(this.endSecondWeekICO + duration.seconds(1));
      beforeContribution = (await this.token.balanceOf(investor));
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor});
      afterContribution = (await this.token.balanceOf(investor));
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(THIRD_WEEK_ICO_RATE.mul(MINIMAL_PURCHASE));

      // fourth week of ICO
      await increaseTimeTo(this.endThirdWeekICO + duration.seconds(1));
      beforeContribution = (await this.token.balanceOf(investor));
      await this.crowdsale.buyTokens(investor, {value: MINIMAL_PURCHASE, from: investor});
      afterContribution = (await this.token.balanceOf(investor));
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(FOURTH_WEEK_ICO_RATE.mul(MINIMAL_PURCHASE));
    });

    it('should reject payments below 0.05 Ether', async function () {
      await increaseTimeTo(this.startTime);
      await this.crowdsale.send(MINIMAL_PURCHASE);
      await this.crowdsale.send(MINIMAL_PURCHASE.minus(1)).should.be.rejectedWith(EVMThrow);
    });

    it('should transfer payments directly to contributionWallet during preICO (noRefundable)', async function () {
      const beforeContribution = web3.eth.getBalance(contributionWallet);
      await increaseTimeTo(this.startTime);
      await this.crowdsale.sendTransaction({value: MINIMAL_PURCHASE, from: investor, gasPrice: 0});
      const afterContribution = web3.eth.getBalance(contributionWallet);
      afterContribution.minus(beforeContribution).should.be.bignumber.equal(MINIMAL_PURCHASE);
    });

    it('should allow finalization and transfer funds to wallet if the goal is reached during ICO', async function () {
      const investmentAmount = ETH_GOAL_ICO;
      await increaseTimeTo(this.startTimeICO);
      await this.crowdsale.sendTransaction({value: investmentAmount, from: purchaser, gasPrice: 0});

      const beforeFinalization = web3.eth.getBalance(contributionWallet);
      await increaseTimeTo(this.afterEndTime);
      await this.crowdsale.finalize({from: owner});
      const afterFinalization = web3.eth.getBalance(contributionWallet);

      afterFinalization.minus(beforeFinalization).should.be.bignumber.equal(investmentAmount);
    });

    it('should allow refunds for ICO participants if the goal is not reached', async function () {
      const balanceBeforeInvestment = web3.eth.getBalance(purchaser);

      await increaseTimeTo(this.startTimeICO);
      await this.crowdsale.sendTransaction({value: ETH_GOAL_PREICO.minus(1), from: purchaser, gasPrice: 0});
      await increaseTimeTo(this.afterEndTime);

      await this.crowdsale.finalize({from: owner});
      await this.crowdsale.claimRefund({from: purchaser, gasPrice: 0}).should.be.fulfilled;

      const balanceAfterRefund = web3.eth.getBalance(purchaser);
      balanceBeforeInvestment.should.be.bignumber.equal(balanceAfterRefund);
    });

  });
