# Deploy

Crowdsale Smart Contract can be found in root folder: *MeeTipCrowdsale_flattened.sol*

## Params for ICO

preICO start: 1508738400 *Is equivalent to: 10/23/2017 @ 6:00am (UTC)*

preICO end: 1509343200

ICO start: 1509948000 *Is equivalent to: 11/06/2017 @ 6:00am (UTC)*

ICO first day end: 1510034400

ICO first week end: 1510552800

ICO second week end: 1511157600

ICO third week end: 1511762400

ICO fourth week end: 1512367200

i.e.

`function MeeTipCrowdsale(
    1508738400,
    1509343200,
    1509948000,
    1510034400,
    1510552800,
    1511157600,
    1511762400,
    1512367200,
    "0x000000ContributionWallet",
    "0x0000000000FoundersWallet",
    "0x000000000000BountyWallet"
    )`

## Usage

After deploy 1500000 MTIP will be sent to foundersWallet and bountyWallet each.

To contribute use `buyTokens()` function of crowdsale contract or send funds directly to crowdsale address.

During preICO funds go directly to contributionWallet.
During ICO funds go to automatically built RefundVault which is managed through Crowdsale contract.

Trigger `finalize()` function when crowdsale is finished. It will send ICO funds to contributionWallet or enable refunds if goal is not reached.

To burn unused bounty tokens trigger `burn(number_of_tokens_to_burn)` function of MeeTip token contract from bountyWallet address.

MeeTip contract address is stored in `token` variable. For example, it can be added in Mist using 'Watch Token' with token address.

# Development

Run `npm install` to install dependencies.

Run testrpc with `bash scripts/testrpc_with_highvalue_accounts.sh`

Run `truffle test`

## Manual testing
Crowdsale can be adjusted for testing with any ICO schedule. For example use 10 minutes interval for each stage. Time in smart contracts presented as unix timestamp  e.g. 1508738400. Use https://www.unixtimestamp.com/ for conversion.

## Flattening
```solidity_flattener --solc-paths=zeppelin-solidity=$(pwd)/node_modules/zeppelin-solidity/ contracts/MeeTipCrowdsale.sol --output MeeTipCrowdsale_flattened.sol```
